import React, { Component } from 'react'

import { Menu, Icon } from 'antd';
import styled from 'styled-components'


import profile from '../img/boy.svg'




const SideBar = styled.div`
  width: 300px;
  background: #25aae1;
  height: 625px;
  float: left;
  h4{
    color: white;
    text-align: center;
    font-size: 20px;
  }
  .circle {
    border-radius: 50%;
	  width: 100px;
    height: 100px;;
    background: white;
    margin: 0 auto;
    margin-top: 30px;
    .profile-img{
      position: absolute;
      top: 155px;
      left: 128px;
      width: 70px;
      height: 70px;
    }
    
    
  }
`
const MenuBar = styled.div`
  position: absolute;

  .Home:hover{
    background-color: #1890ff;
  }
  .Company:hover{
    background-color: #1890ff;
  }
  .User:hover{
    background-color: #1890ff;
  }

`



class Sidebar extends Component{
    render() {
        return (
          <div>
            <SideBar>
            <div className="circle"><img src={profile} className="profile-img" alt="profile"/></div>
            <h4>ADMIN</h4>
            <MenuBar style={{ width: 300 }}>        
              <Menu                                
                mode="inline"
                theme="dark"          
              >
                
                <Menu.Item key="1" className="Home"> 
                  <a href="/">                 
                  <Icon type="home" /><span>Home</span>                  
                  </a>
                </Menu.Item>
                
                
                
                <Menu.Item key="2" className="Company">
                  <a href="/company">
                  <Icon type="global" /><span>Company</span>
                  </a> 
                </Menu.Item>
                
                <Menu.Item key="3" className="User">
                  <a href="/user">
                  <Icon type="team" />
                  <span> User</span>
                  </a>
                </Menu.Item>  
                
              </Menu>
            </MenuBar>
            </SideBar>
        </div>)
    }
}

export default Sidebar