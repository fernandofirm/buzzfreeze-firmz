import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Modal, Input , Button, Icon, Table, Card} from 'antd';

import styled from 'styled-components'


const Wrapper = styled.div`
float:left;

margin-top: 30px;
margin-left: 160px;
padding: 50px;
.Company {
    margin-bottom: 40px;
    border-bottom: 5px solid #1890ff;
    border-radius: 10px;
    p {
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        color: #1890ff
    }
}
.Company:hover{
    border-top: 0.5px solid #1890ff;
    border-left: 0.5px solid #1890ff;
    border-right: 0.5px solid #1890ff;
    border-radius: 10px;
}
.User{
    border-bottom: 5px solid #1890ff;
    border-radius: 10px;
    p {
        text-align: center;
        font-size: 20px;
        font-weight: bold;
        color: #1890ff
    }
}
.User:hover{
    border-top: 0.5px solid #1890ff;
    border-left: 0.5px solid #1890ff;
    border-right: 0.5px solid #1890ff;
    border-radius: 10px;
}

`

class Home extends Component {
    render() {
        return (
            <Wrapper>
                <Link to="/company">
                <Card style={{ width: 600 }} className="Company">
                <p>Company</p>             
                </Card>
                </Link>

                <Link to="/user">
                <Card style={{ width: 600 }} className="User">
                <p>User</p>               
                </Card>
                </Link>

            </Wrapper>
        )
    }
}

export default Home