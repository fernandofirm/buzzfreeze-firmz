import React , { Component }from 'react'

import { Table, Input, Icon, Button, Popconfirm } from 'antd';

import styled from 'styled-components'

const Wrapper = styled.div`
float: left;
.dataTable {
    margin-left: 100px;
    margin-top: 70px;
}
`


class ListCollection extends Component {
    
        constructor(props) {
          super(props);
          this.columns = [{
            title: 'ประเภท',
            dataIndex: 'type',
            width: '20%',
            // render: (text, record) => (
            //   <EditableCell
            //     value={text}
            //     onChange={this.onCellChange(record.key, 'name')}
            //   />
            // ),
          }, {
            title: 'จำนวนโปรแกรมเมอร์',
            dataIndex: 'countProgrammer',
          }, {
            title: 'จำนวนบริษัท',
            dataIndex: 'CountCompany',
          },
          {
            title: 'คีเวิร์ด',
            dataIndex: 'keyword'
          }

          
        //   {
        //     title: 'operation',
        //     dataIndex: 'operation',
        //     render: (text, record) => {
        //       return (
        //         this.state.dataSource.length > 1 ?
        //         (
        //           <Popconfirm title="Sure to delete?" onConfirm={() => this.onDelete(record.key)}>
        //             <a href="#">Delete</a>
        //           </Popconfirm>
        //         ) : null
        //       );
        //     },
        //   }
        ];
      
          this.state = {
            dataSource: [{
              key: '0',
              type: 'Facebook 0',
              countProgrammer: 10,
              CountCompany: 20,
              keyword: 'NodeJs'
            }, {
              key: '1',
              type: 'Facebook 1',
              countProgrammer: 10,
              CountCompany: 20,
              keyword: 'Javascript'
            }],
            count: 2,
          };
        }
        onCellChange = (key, dataIndex) => {
          return (value) => {
            const dataSource = [...this.state.dataSource];
            const target = dataSource.find(item => item.key === key);
            if (target) {
              target[dataIndex] = value;
              this.setState({ dataSource });
            }
          };
        }
        onDelete = (key) => {
          const dataSource = [...this.state.dataSource];
          this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
        }
        handleAdd = () => {
          const { count, dataSource } = this.state;
          const newData = {
            key: count,
            type: `Facebook ${count}`,
            countProgrammer: `${10 + count}`,
            CountCompany: `${20 + count}`,
            keyword: `React ${count}`
          };
          this.setState({
            dataSource: [...dataSource, newData],
            count: count + 1,
          });
        }
        render() {
          const { dataSource } = this.state;
          const columns = this.columns;
          return (
            <div>
              <Button className="editable-add-btn" onClick={this.handleAdd}>ADD Keyword</Button>
              <Table bordered dataSource={dataSource} columns={columns} />
            </div>
          );
        }
      }

  export default ListCollection